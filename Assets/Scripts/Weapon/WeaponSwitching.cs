using UnityEngine;

public class WeaponSwitching : MonoBehaviour
{
    #region Singleton

    public static WeaponSwitching instance;

    private void Awake()
    {
        if (instance != null)
            return;
        instance = this;
    }

    #endregion
    public int selectedWeapon = 0;

    private void Start()
    {
        SelectWeapon();
    }

    private void Update()
    {
        var previousSelectedWeapon = selectedWeapon;
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (selectedWeapon >= transform.childCount - 1)
                selectedWeapon = 0;
            else
                selectedWeapon++;
        }

        if (previousSelectedWeapon != selectedWeapon)
            SelectWeapon();
    }

    private void SelectWeapon()
    {
        int i = 0;
        foreach (Transform weapon in transform)
        {
            if (i == selectedWeapon)
                weapon.gameObject.SetActive(true);
            else weapon.gameObject.SetActive(false);
            i++;
        }
    }
}