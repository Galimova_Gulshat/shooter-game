﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

public class Gun : MonoBehaviour
{
    [SerializeField]
    private float damage = 10f;
    [SerializeField]
    private float range = 100f;
    [SerializeField]
    private Camera fpsCamera;
    [SerializeField]
    private float reloadTime = 1f;
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private GameObject firePoint;
    
    private int currentAmmo;
    private bool GameIsPaused = false;  
    private bool isRealoding = false;
    [SerializeField] private float bulletLife = 1f;
    /// <summary>
    /// 
    /// </summary>
    /// 
    public AudioClip loseShootSound;
    public AudioClip shootSound;
    
    protected ObjectPooling pool;
    protected static GameObject objectsParent;
    [SerializeField]
    protected int poolObjCount = 10;
    
    [SerializeField]
    private IPoolObject vfx;
    /// <summary>
    /// 
    /// </summary>
    private void Awake()
    {
        if (vfx == null)
            poolObjCount = 0;
        objectsParent = new GameObject();
        objectsParent.name = "BulletPool";
        if (vfx != null)
        {
            pool = new ObjectPooling();
            pool.Initialize(poolObjCount, vfx, objectsParent.transform);
        }
    }
    
    public GameObject GetObject(Vector3 position, Quaternion rotation)
    {
        GameObject result = null;
        result = pool.GetObject().gameObject;
        result.transform.position = position;
        result.transform.rotation = rotation;
        result.SetActive(true);
        return result;
    }

    private void Start()
    {
        /*objectsParent.name = "BulletPool";*/
        currentAmmo = poolObjCount;
        UIController.instance.ShowCurrentAmmo(currentAmmo);
    }



    private void OnEnable()
    {
        isRealoding = false;
        animator.SetBool("Reloading", false);
        UIController.instance.ShowCurrentAmmo(currentAmmo);
    }

    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (isRealoding || GameIsPaused)
            return;
        
        if (Input.GetKeyDown(KeyCode.E))
            StartCoroutine(Reload());
        
        if (currentAmmo <= 0)
            return;

        if (Input.GetButtonDown("Fire1"))
            Shoot();
    }

    void Shoot()
    {
        GetComponent<AudioSource>().PlayOneShot(shootSound);
        currentAmmo--;
        UIController.instance.ShowCurrentAmmo(currentAmmo);
        RaycastHit hit;
        if (Physics.Raycast(fpsCamera.transform.position, fpsCamera.transform.forward, out hit, range))
        {
            var target = hit.transform.GetComponent<CharacterStats>();
            if (target != null)
                target.TakeDamage(damage);               
            SpawnVFX();
        }
    }

    IEnumerator Reload()
    {
        isRealoding = true;
        animator.SetBool("Reloading", true);
        yield return new WaitForSeconds(reloadTime - 0.2f);
        animator.SetBool("Reloading", false);
        yield return new WaitForSeconds(0.2f);
        currentAmmo = poolObjCount;
        isRealoding = false;
        UIController.instance.ShowCurrentAmmo(currentAmmo);
    }
    
    void SpawnVFX()
    {
        GameObject vfx;

        if (firePoint != null)
        {
            vfx = GetObject(firePoint.transform.position, firePoint.transform.rotation); 
            StartCoroutine(ReturnBullet(vfx));
        }
    }
    
    IEnumerator ReturnBullet(GameObject bullet)
    {
        yield return new WaitForSeconds(bulletLife);
        bullet.GetComponent<ProjectileMove>().ReturnToPool();
    }
    
    public void GetBullet()
    {
        StartCoroutine(Reload());   
    }

    public void PauseGame()
    {
        GameIsPaused = true;
    }
    
    public void PlayGame()
    {
        GameIsPaused = false;
    }
}
