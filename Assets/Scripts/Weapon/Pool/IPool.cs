using UnityEngine;

public class IPool : MonoBehaviour
{
    protected ObjectPooling pool;
    protected static GameObject objectsParent;
    [SerializeField]
    protected int poolObjCount = 10;
    
    [SerializeField]
    private IPoolObject vfx;
    
    private void Awake()
    {
        if (vfx == null)
            poolObjCount = 0;
        objectsParent = new GameObject();
        if (vfx != null)
        {
            pool = new ObjectPooling();
            pool.Initialize(poolObjCount, vfx, objectsParent.transform);
        }
    }
    
    public GameObject GetObject(Vector3 position, Quaternion rotation)
    {
        GameObject result = null;
        result = pool.GetObject().gameObject;
        result.transform.position = position;
        result.transform.rotation = rotation;
        result.SetActive(true);
        return result;
    }
}