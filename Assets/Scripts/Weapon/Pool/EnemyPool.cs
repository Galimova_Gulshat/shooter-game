using System.Collections;
using UnityEngine;

public class EnemyPool : MonoBehaviour
{
    #region Singleton

    public static EnemyPool instance;


    #endregion
    
    [SerializeField] private float time = 4f;
    
    public ObjectPooling pool;
    public static GameObject objectsParent;
    [SerializeField]
    protected int poolObjCount = 10;
    
    [SerializeField]
    private IPoolObject vfx;
    private void Start()
    {
        /*if (vfx == null)
            poolObjCount = 0;
        objectsParent = new GameObject();
        objectsParent.name = "EnemiesPool";
        if (vfx != null)
        {
            pool = new ObjectPooling();
            pool.Initialize(poolObjCount, vfx, objectsParent.transform);
        }*/
        //objectsParent.name = "EnemyPool";
        
        
    }
    
    private void Awake()
    {
        if (instance != null)
            return;
        instance = this;
        if (vfx == null)
            poolObjCount = 0;
        objectsParent = new GameObject();
        //objectsParent.name = "EnemiesPool";
        if (vfx != null)
        {
            pool = new ObjectPooling();
            pool.Initialize(poolObjCount, vfx, objectsParent.transform);
        }
        
    }
    
    public GameObject GetObject(Vector3 position, Quaternion rotation)
    {
        GameObject result = null;
        result = pool.GetObject().gameObject;
        result.transform.position = position;
        result.transform.rotation = rotation;
        result.SetActive(true);
        return result;
    }

    public void SpawnEnemy(Transform position, int count)
    {
        for (int i = 0; i < count; i++)
        {
            StartCoroutine(CreateEnemy(position));
        }
    }

    
    IEnumerator CreateEnemy(Transform position)
    {
        yield return new WaitForSeconds(time);
        var v = GetObject(position.transform.position, position.transform.rotation); 
        v.GetComponent<FolllowingMob>().StartMoving();
    }

}