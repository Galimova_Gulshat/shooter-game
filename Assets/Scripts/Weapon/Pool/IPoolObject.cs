
using UnityEngine;

public class IPoolObject : MonoBehaviour
{
    protected bool isActive;
    
    public void Active()
    {
        isActive = true;
    }
    
    public void ReturnToPool()
    {
        gameObject.SetActive(false);
        isActive = false;
    }
}