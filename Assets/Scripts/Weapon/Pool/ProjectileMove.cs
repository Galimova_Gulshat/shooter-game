﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMove : IPoolObject
{
    public float speed;
    public float fireRate;
    
    void Update()
    {
        if (speed != 0)
        {
            transform.position += transform.forward * (speed * Time.deltaTime);
        }
    }

    /*private void OnTriggerEnter(Collider other)
    {
        if(!other.tag.Equals("Weapon") && !other.tag.Equals("Player"))
            ReturnToPool();
    }*/
}
