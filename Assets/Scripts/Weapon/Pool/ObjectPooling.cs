using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling
{
    private List<IPoolObject> objects;
    private Transform objectsParent;

    public void Initialize(int count, IPoolObject sample, Transform objects_parent)
    {
        objects = new List<IPoolObject>();
        objectsParent = objects_parent;
        for (int i = 0; i < count; i++)
            AddObject(sample, objects_parent);
    }


    public IPoolObject GetObject()
    {
        for (int i = 0; i < objects.Count; i++)
        {
  
            if (objects[i].gameObject.activeInHierarchy == false)
            {
                //objects[i].Active();
                return objects[i];
            }
        }
        AddObject(objects[0], objectsParent);
        //objects[objects.Count - 1].Active();
        return objects[objects.Count - 1];
    }

    void AddObject(IPoolObject sample, Transform objects_parent)
    {
        GameObject temp;
        temp = GameObject.Instantiate(sample.gameObject);
        temp.name = sample.name;
        temp.transform.SetParent(objects_parent);
        objects.Add(temp.GetComponent<IPoolObject>());
        temp.SetActive(false);
    }
}