using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    [SerializeField] protected float radius;
    protected bool isFollowed = false;
    protected bool hasInteracted;

    public abstract void Interact();
}