using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class SpiritController : Interactable
{
    private IAnimator animator;
    private GameObject player;
    private NavMeshAgent agent;
    private bool isActive = false;
    [SerializeField] private float distance = 1f;

    private void Start()
    {
        player = GameManager.instance.player;
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<IAnimator>();
    }

    private void Update()
    {
        if (isActive)
        {
            agent.SetDestination(player.transform.position);
            /*if (Vector3.Distance(transform.position, player.transform.position) >= distance)
                agent.SetDestination(player.transform.position);
            else agent.SetDestination(transform.position);*/
            
        }
        //agent.SetDestination(player.transform.position);
    }

    public override void Interact()
    {
        animator.Attack();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            //agent.SetDestination(transform.position);
            //agent.Stop();
            transform.LookAt(player.transform.position);
            Interact();
        }
    }

    public void SetActive()
    {
        isActive = true;
        Debug.Log(isActive);
    }

    public void DisActive()
    {
        isActive = false;
    }
}