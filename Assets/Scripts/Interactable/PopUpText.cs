using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PopUpText : Interactable
{
    [SerializeField] private GameObject mainText;
    [SerializeField] private Text positiveText;
    [SerializeField] private Text neutralText;
    [SerializeField] private Text negativeText;
    [SerializeField] private FolllowingMob police;
    
    public delegate void OnAnswerChanged();

    public OnAnswerChanged onAnswerChangedCallback;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Player") && !hasInteracted)
            Interact();
    }

    public override void Interact()
    {
        hasInteracted = true;
        Time.timeScale = 0f;
        GameManager.instance.player.GetComponentInChildren<Gun>().PauseGame();
        mainText.SetActive(true);
    }

    private void CloseMainText()
    {
        Time.timeScale = 1f;
        GameManager.instance.player.GetComponentInChildren<Gun>().PlayGame();
        mainText.SetActive(false);
    }

    public void OnRightAnswer()
    {
        Button(positiveText.gameObject);
        onAnswerChangedCallback += () =>
            GameManager.instance.player.GetComponent<CharacterStats>()
                .ChangeArmor(new Modifier(1));
    }

    public void OnNeutralAnswer() =>
        Button(neutralText.gameObject);
    
    public void OnWrongAnswer()
    {
        Button(negativeText.gameObject);
        onAnswerChangedCallback += () =>  GameManager.instance.KillPlayer();     
    }

    private void Button(GameObject gameObj)
    {
        CloseMainText();
        gameObj.SetActive(true);
        StartCoroutine(CloseAnswer(gameObj));
    }

    private IEnumerator CloseAnswer(GameObject text)
    {
        yield return new WaitForSeconds(5f);
        text.SetActive(false);
        if(onAnswerChangedCallback != null)
            onAnswerChangedCallback.Invoke();
    }
}