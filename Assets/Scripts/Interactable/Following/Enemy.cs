using UnityEngine;

public class Enemy : FolllowingMob
{
    private float lastAttackTime;
    [SerializeField] private float attackTime = 2f;
    private float attack;
    

    private void Awake()
    {
        
        attack = GetComponentInChildren<CharacterStats>().Damage;
    }

    public override void Interact()
    {
        
        if (attackTime + lastAttackTime <= Time.time)
        {
            animator.Attack();
            target.GetComponent<CharacterStats>().TakeDamage(attack);
            lastAttackTime = Time.time;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag.Equals("Player"))
            Interact();
    }
}