using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PoliceController : FolllowingMob
{
    [SerializeField] private float readingTime = 1.5f;
    [SerializeField] private Text text;
    public bool IsFollowing => isFollowed;
    public override void Interact()
    {
        if (isFollowed)
        {
            text.gameObject.SetActive(true);
            StartCoroutine(LoadScene());
        }
    }

    private IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(readingTime);
        GameManager.instance.KillPlayer();
    }
}