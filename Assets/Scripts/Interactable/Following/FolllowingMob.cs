using UnityEngine;
using UnityEngine.AI;

public class FolllowingMob : Interactable
{
    private NavMeshAgent agent;
    private bool isActive;
    protected GameObject target;
    [SerializeField] private float speed;
    protected RCharAnimation animator;
    
    public AudioClip enemyAppearSound;

    public float Speed => speed;

    private void Start()
    {
        animator = GetComponentInChildren<RCharAnimation>();
        target = GameManager.instance.player;
        agent = GetComponent<NavMeshAgent>();
    }

    public override void Interact()
    {
    }
    
    public void Update()
    {
        if (isActive && isFollowed)
        {
            //transform.forward = target.transform.position - transform.position;
            //transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
            agent.SetDestination(target.transform.position);
            
        }
           
    }
    
    public void StartMoving()
    {
        GetComponent<AudioSource>().PlayOneShot(enemyAppearSound);
        isActive = true;
        isFollowed = true;
        /*if(animator != null)
            animator.Run();*/
    }

    public void StopMoving()
    {
        isActive = false;
        /*if(animator != null)
            animator.Idle();*/
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            //transform.position = transform.position;
            agent.SetDestination(transform.position);
            agent.Stop();
            
            //animator.Idle();
            Interact();
            isFollowed = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            isFollowed = true;
            //animator.Run();
        }
    }
}