using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {

            GameManager.instance.SetCheckpoint(this.transform, GameManager.instance.player.GetComponent<PlayerStats>().Health);
        }
    }
}