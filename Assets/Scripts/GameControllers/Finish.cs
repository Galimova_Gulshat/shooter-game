using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Finish : MonoBehaviour
{
    [SerializeField] private GameObject finishText;
    [SerializeField] private float readingTime = 1.5f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            finishText.SetActive(true);
            StartCoroutine(FinishGame());
        }
    }

    IEnumerator FinishGame()
    {
        yield return new WaitForSeconds(readingTime);
        GameManager.instance.Finish();
    }
}