using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIController : MonoBehaviour
{
    #region Singleton

    public static UIController instance;

    private void Awake()
    {
        if (instance != null)
            return;
        instance = this;
    }

    #endregion

    [SerializeField] private string armor = "Защита: ";
    [SerializeField] private string currentBullet = "Число пуль: ";
    
    [SerializeField] private Text armorText;
    [SerializeField] private Text winningText;
    [SerializeField] private Text startText;
    [SerializeField] private Text currentBulletText;
    [SerializeField] private Text spiritText;
    [SerializeField] private Image healthBar;

    private void Start()
    {
        startText.gameObject.SetActive(true);
        StartCoroutine(CloseText());
    }

    private IEnumerator CloseText()
    {
        yield return new WaitForSeconds(2f);
        startText.gameObject.SetActive(false);
    }

    public void ShowPlayerHealth(float curHealth, float maxHealth)
    {
        healthBar.fillAmount = curHealth / maxHealth;
    }

    public void ShowArmorStat(float armorValue)
    {
        armorText.text = armor + armorValue;
    }

    public void ShowCurrentAmmo(int currentAmmo)
    {
        currentBulletText.text = currentBullet + currentAmmo;
    }

    public void Winning()
    {
        winningText.gameObject.SetActive(true);
    }

    public void ShowSpiritText()
    {
        spiritText.gameObject.SetActive(true);
        StartCoroutine(KillPlayer());
    }

    private IEnumerator KillPlayer()
    {
        yield return new WaitForSeconds(2f);
        GameManager.instance.KillPlayer();
    }
}