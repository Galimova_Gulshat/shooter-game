using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region Singleton

    public static GameManager instance;

    private void Awake()
    {
        if (instance != null)
            return;
        instance = this;
    }

    #endregion

    public GameObject player;
    private Transform checkPoint;
    private float prevHealth;

    private void Start()
    {
        SetCheckpoint(transform, player.GetComponent<CharacterStats>().Health);
    }

    public void KillPlayer()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Finish()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void SetCheckpoint(Transform newCheckpoint, float currHealth)
    {
        checkPoint = newCheckpoint;
        prevHealth = currHealth;
    }

    public void RespawnPlayer()
    {
        player.transform.position = checkPoint.position;
        player.transform.rotation = checkPoint.rotation;
        var playerStats = player.GetComponent<PlayerStats>();
        playerStats.ChangeHealth(new Modifier(prevHealth - playerStats.Health));
    }
}