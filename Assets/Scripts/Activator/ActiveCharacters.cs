
    using UnityEngine;

    public class ActiveCharacters : MonoBehaviour
    {
        [SerializeField] private Transform enemyPosition;
        [SerializeField] private int enemyCount;
        private EnemyPool enemyPool;

        private void Start()
        {
            enemyPool = EnemyPool.instance;
             //FindObjectOfType <EnemyPool>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                enemyPool.SpawnEnemy(enemyPosition, enemyCount);
                /*foreach (var enemy in enemies)
                    enemy.StartMoving();*/
            }
        }
        
        private void OnTriggerExit(Collider other)
        {
            /*if (other.tag == "Player")
            {
                foreach (var enemy in enemies)
                    enemy.StopMoving();
            }*/
        }
    }