
using UnityEngine;

public class ActiveSpirit : MonoBehaviour
{
    [SerializeField] private SpiritController spirit;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            spirit.SetActive();
            Debug.Log("Active spirit");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag.Equals("Player"))
            spirit.DisActive();
    }
}