using UnityEngine;

public class ActivationPolice : MonoBehaviour
{
    [SerializeField] private PoliceController policeman;

    private void OnTriggerExit(Collider other)
    {
        if(other.tag.Equals("Player"))
            policeman.StopMoving();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(policeman.IsFollowing)
            policeman.StartMoving();
    }
}