using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    [SerializeField] private GameObject inventoryUI;
    [SerializeField] private Transform itemsParent;

    private ItemInventory inventory;
    private InventorySlot[] slots;

    private void Start()
    {
        inventory = ItemInventory.instance;
        inventory.onItemChangedCallback += UpdateUI;
        slots = itemsParent.GetComponentsInChildren<InventorySlot>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Inventory"))
            inventoryUI.SetActive(!inventoryUI.activeSelf);
    }

    void UpdateUI()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            
            for (int j = 0; j < inventory.items.Count; j++)
            {
                if (slots[i].item == inventory.items[j])
                {
                    if (slots[i].item != null)
                    {
                        slots[i].SetCount(inventory.items.FindAll(item => item == slots[i].item).Count);
                    }
                }

            }   
            if (i < inventory.items.Count)
            {
                slots[i].AddItem(inventory.items[i]);
                if(slots[i].item != null)
                    slots[i].SetCount(inventory.items.FindAll(item => item == slots[i].item).Count);
            }
                
            else
                slots[i].ClearSlot();
            
            
        }
    }
}