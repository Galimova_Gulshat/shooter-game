using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public Button removeButton;
    private int count = 0;
    public Image icon;
    public Item item;
    public Text text;

    public void AddItem(Item newItem)
    {
        item = newItem;
        icon.sprite = item.Icon;
        icon.enabled = true;
        removeButton.interactable = true;
        //SetCount();
    }

    public void ClearSlot()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
        removeButton.interactable = false;
        ClearCount();
    }

    public void SetCount(int newCount)
    {
        count = newCount;
        text.text = count.ToString();
    }

    public void ClearCount()
    {
        count = 0;
        text.text = count.ToString();
    }

    public void OnRemoveButton()
    {
        ItemInventory.instance.RemoveItem(item);
    }

    public void UseItem()
    {
        if (item != null && icon.sprite != null)
            item.Use();
    }
}