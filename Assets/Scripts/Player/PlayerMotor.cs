using UnityEngine;


public class PlayerMotor : MonoBehaviour
{
    private Vector3 m_Velocity = Vector3.zero;
    private Vector3 m_Rotation = Vector3.zero;
    private Vector3 m_CameraRotation = Vector3.zero;
    private Rigidbody m_Rb;
    [SerializeField] private float minRotation = 3f;
    [SerializeField] private float maxRotation = 5f;
    [SerializeField] private new Camera camera;

    private void Start()
    {
        m_Rb = GetComponent<Rigidbody>();
    }

    public void Move(Vector3 velocity)
    {
        this.m_Velocity = velocity;
    }

    private void FixedUpdate()
    {
        PerformMovement();
        PerformRotation();
    }

    private void PerformMovement()
    {
        if (m_Velocity != Vector3.zero)
            m_Rb.MovePosition(m_Rb.position + m_Velocity * Time.fixedDeltaTime);
    }

    private void PerformRotation()
    {
        if (m_Rotation != Vector3.zero)
            m_Rb.MoveRotation(m_Rb.rotation * Quaternion.Euler(m_Rotation));
        if (camera != null)
            camera.transform.Rotate(-m_CameraRotation);
    }

    public void Rotate(Vector3 rotation)
    {
        m_Rotation = rotation;
    }

    public void RotateCamera(Vector3 cameraRotation)
    {
        var x = Mathf.Clamp(cameraRotation.x, minRotation, maxRotation);
        cameraRotation.x = x;
        m_CameraRotation = cameraRotation;
    }
}