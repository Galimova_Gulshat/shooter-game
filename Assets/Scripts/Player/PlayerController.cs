using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private float rot = 3f;
    [SerializeField] private float jumpForce = 3f;
    [SerializeField] private float maxY = 3f;
    
    public float gravityScale = 3f;
    private Rigidbody rb;
    private PlayerMotor motor;
    
    public AudioClip walkSound;
    public AudioClip jumpSound;
    

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        motor = GetComponent<PlayerMotor>();
    }
    

    private void Update()
    {
        if (transform.position.y >= maxY)
        {
            rb.useGravity = true;
            StartCoroutine(SetGravity());
        }
        if (EventSystem.current.IsPointerOverGameObject())
        {
            motor.Rotate(Quaternion.identity.eulerAngles);
            motor.RotateCamera(Quaternion.identity.eulerAngles);
            return;
        }

        var xMove = Input.GetAxisRaw("Horizontal");
        var zMove = Input.GetAxisRaw("Vertical");

        var moveHor = -transform.right * xMove;
        var moveVer = -transform.forward * zMove;

        var velocity = (moveHor + moveVer).normalized * speed;
        motor.Move(velocity);

        var yRot = Input.GetAxisRaw("Mouse X");
        var _rotation = new Vector3(0f, yRot, 0f) * rot;

        motor.Rotate(_rotation);

        var xRot = Input.GetAxisRaw("Mouse Y");
        var _cameraRotation = new Vector3(xRot, 0f, 0f) * rot;

        motor.RotateCamera(_cameraRotation);

        if (Input.GetButtonDown("Jump"))
            motor.Move(transform.up * jumpForce);
    }

    private IEnumerator SetGravity()
    {
        yield return new WaitForSeconds(2f);
        rb.useGravity = false;
    }
}