using UnityEngine;

public class PlayerStats : CharacterStats
{
    private UIController uiController;

    private void Start()
    {
        uiController = UIController.instance;
        onStatChangedCallback += ShowPlayerStats;
        onStatChangedCallback.Invoke();     
    }

    private void Update()
    {
        if (lastRegTime + regenerationTime <= Time.time && health.GetValue() < maxHealth)
        {
            Heal();
            lastRegTime = Time.time;
            if(onStatChangedCallback != null)
                onStatChangedCallback.Invoke();
        }
    }

    public void ShowPlayerStats()
    {
        uiController.ShowPlayerHealth((float) System.Math.Round(health.GetValue(), 2), maxHealth);
        uiController.ShowArmorStat(armor.GetValue());
    }
}