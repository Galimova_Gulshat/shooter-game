using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

public class CharacterStats : MonoBehaviour
{
    [SerializeField] protected int maxHealth = 100;
    [SerializeField] protected float regenerationTime = 3f;

    protected float lastRegTime;
    [SerializeField]
    protected Stat health;
    [SerializeField]
    protected Stat damage;
    [SerializeField]
    protected Stat armor;
    [SerializeField]
    protected Stat regeneration;
    [SerializeField]
    private IAnimator animator;
    
    public delegate void OnStatChanged();

    public OnStatChanged onStatChangedCallback;

    public float Damage => damage.GetValue();

    public float Health => health.GetValue();

    private void Awake()
    {
        
        if(animator == null)
            animator = GetComponent<IAnimator>();
        health.AddModifier(new Modifier(maxHealth));
        if (regeneration.GetValue() == 0)
            regeneration.AddModifier(new Modifier(0.001f * Time.deltaTime));
    }

    public void TakeDamage(float _damage)
    {
        _damage -= armor.GetValue();
        _damage = _damage <= 0 ? 0 : _damage;
        health.AddModifier(new Modifier(-_damage));
        if(animator != null)
            animator.TakeDamage();
        if (health.GetValue() <= 0)
        {
            Die();
            return;
        }

        if(onStatChangedCallback != null)
            onStatChangedCallback.Invoke();
        if(animator != null)
            animator.TakeDamage();
    }

    public void ChangeHealth(Modifier value)
    {
        health.AddModifier(value);
        if(onStatChangedCallback != null)
            onStatChangedCallback.Invoke();
    }
    
    public void ChangeArmor(Modifier value)
    {
        armor.AddModifier(value);
        if(onStatChangedCallback != null)
            onStatChangedCallback.Invoke();
    }

    private void Die()
    {
        if(animator != null)
            animator.Die();
        if (gameObject.tag.Equals("Player")) 
            GameManager.instance.RespawnPlayer();
        else if (gameObject.tag.Equals("Spirit"))
        {
            UIController.instance.ShowSpiritText();
        }
        else
        {
            animator.Die();
            StartCoroutine(DieAnim());
        }
    }

    private IEnumerator DieAnim()
    {
        yield return new WaitForSeconds(2f);
        gameObject.SetActive(false);
    }

    public void Heal()
    {
        health.AddModifier(new Modifier(regeneration.GetValue()));
        var diff = maxHealth - health.GetValue();
        if (diff < 0)
            health.AddModifier(new Modifier(diff));
    }
}