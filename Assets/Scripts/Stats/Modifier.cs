using UnityEngine;

[System.Serializable]
public class Modifier
{
    [SerializeField] public float Value;

    public Modifier(float value)
    {
        Value = value;
    }
}