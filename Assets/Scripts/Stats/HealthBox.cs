using UnityEngine;

public class HealthBox : Interactable
{
    [SerializeField] private Modifier healthValue;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Player"))
            Interact();
    }

    public override void Interact()
    {
        if (!hasInteracted)
        {
            GameManager.instance.player.GetComponent<CharacterStats>().ChangeHealth(healthValue);
            hasInteracted = true;
        }
    }
}