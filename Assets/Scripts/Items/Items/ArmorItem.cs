using UnityEngine;

public class ArmorItem : Item
{
    [SerializeField] private Modifier armorValue;

    public override void Use()
    {
        GameManager.instance.player.GetComponent<CharacterStats>().ChangeArmor(armorValue);
        RemoveFromInventory();
    }
}