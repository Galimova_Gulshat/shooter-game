using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    [SerializeField] private string description;
    [SerializeField] private Sprite icon;

    public Sprite Icon => icon;

    public virtual void Use()
    {
        Debug.Log(name);
        RemoveFromInventory();
    }

    public void RemoveFromInventory()
    {
        ItemInventory.instance.RemoveItem(this);
    }
}