using UnityEngine;


[CreateAssetMenu(fileName = "Health", menuName = "Inventory/Health")]
public class HealItem : Item
{
    [SerializeField] private Modifier healValue = new Modifier(0);

    public override void Use()
    {
        GameManager.instance.player.GetComponent<CharacterStats>().ChangeHealth(healValue);
        RemoveFromInventory();
    }

}