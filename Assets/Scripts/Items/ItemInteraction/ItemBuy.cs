using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ItemBuy : Interactable
{
    [SerializeField] private Item item;
    [SerializeField] private new string name;
    [SerializeField] private Text text;

    public override void Interact()
    {
        if (!hasInteracted)
        {
            ShowText();
            Buy();
        }
    }

    private void Buy()
    {
        ItemInventory.instance.ReplaceItems(item, name);
        hasInteracted = true;
    }

    private void ShowText()
    {
        text.text = "Give me " + name + " and get " + item.name;
        StartCoroutine(CloseText());
    }

    private IEnumerator CloseText()
    {
        yield return new WaitForSeconds(2);
        text.text = string.Empty;
    }
}