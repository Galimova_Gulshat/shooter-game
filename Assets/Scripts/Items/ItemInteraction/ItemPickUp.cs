using UnityEngine;

public class ItemPickUp : Interactable
{
    public Item item;
    
    public AudioClip itemPickUpSound;

    public override void Interact()
    {
        PickUp();
    }

    private void PickUp()
    {
        
        if (!hasInteracted)
        {
            var s = GetComponent<AudioSource>();
            s.PlayOneShot(s.clip);
            Debug.Log(s.clip.name);
            hasInteracted = true;
            var wasPickedUp = ItemInventory.instance.AddItem(item);
            if (wasPickedUp)
                this.gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Player"))
            Interact();
    }
}