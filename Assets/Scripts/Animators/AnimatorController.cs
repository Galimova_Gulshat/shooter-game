using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class AnimatorController : IAnimator
{
    Animator animator;
    private NavMeshAgent agent;
    private float curSpeed;
    private float speed;

    private void Start()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        speed = agent == null ? GetComponent<FolllowingMob>().Speed : agent.speed;
    }

    private void Update()
    {
        Run();
    }

    public override void TakeDamage()
         {
             animator.SetBool("Damage", true);
             StartCoroutine(StopTakeDamage());
         }
     
         public override void Attack()
         {
             animator.SetBool("Attack", true);
             StartCoroutine(StopAttack());
         }
         
         private IEnumerator StopTakeDamage()
         {
             yield return new WaitForSeconds(1.5f);
             animator.SetBool("Damage", false);
         }
     
         private IEnumerator StopAttack()
         {
             yield return new WaitForSeconds(1.5f);
             animator.SetBool("Attack", false);
         }

    public override void Run()
    {
        if(agent == null)
            curSpeed = transform.position.magnitude / speed;
        else curSpeed = agent.velocity.magnitude / speed;
        animator.SetFloat("Speed", curSpeed);
    }

    public override void Die()
    {
        animator.SetBool("Die", true);
        StartCoroutine(StopDie());
    }
    
    private IEnumerator StopDie()
    {
        yield return new WaitForSeconds(1.5f);
        animator.SetBool("Die", false);
    }
}