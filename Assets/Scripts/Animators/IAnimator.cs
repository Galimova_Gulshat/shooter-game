using UnityEngine;

public abstract class IAnimator : MonoBehaviour
{
    public abstract void TakeDamage();
    public abstract void Attack();
    public abstract void Run();

    public abstract void Die();
}