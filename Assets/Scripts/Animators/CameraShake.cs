using System.Collections;
using UnityEngine;

public class CameraShake : IAnimator
{
    [SerializeField] private float dur = 0.15f;
    [SerializeField] private float mag = 0.15f;
    
    public override void TakeDamage()
    {
        StartCoroutine(Shake(dur, mag));
    }
    
    public override void Attack() {}
    public override void Run() {}
    
    public override void Die() {}
    
    public IEnumerator Shake(float duration, float magnitude)
    {
        var originalPos = transform.localPosition;
        var elapsed = 0.0f;
        while (elapsed < duration)
        {
            var x = Random.Range(-1f, 1f) * magnitude;
            var y = Random.Range(-1f, 1f) * magnitude;
            transform.localPosition = new Vector3(x, originalPos.y, y);
            elapsed += Time.deltaTime;
            yield return null;
        }

        transform.localPosition = originalPos;
    }
}