using UnityEngine;

namespace DefaultNamespace
{
    public class Map : MonoBehaviour
    {
        private Transform player;

        private void Start()
        {
            player = GameManager.instance.player.transform;
        }

        private void LateUpdate()
        {
            var newPosition = player.position;
            newPosition.y = transform.position.y;
            transform.position = newPosition;
            transform.rotation = Quaternion.Euler(90f, player.eulerAngles.y, 0f);
        }
    }
}