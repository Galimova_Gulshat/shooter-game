﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RCharAnimation : IAnimator
{
    Animator animator;
    private NavMeshAgent agent;
    private float curSpeed;
    private float speed;
    void Start()
    {
        animator = GetComponentInParent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    void Update() //Тут метод Update пока комменти, если все будет работать удалишь. Вместо этого активируешь следующие методы в других скриптах
    {
        Run();
        /*RunAnim();
        DieAnim();
        DamageAnim();
        AttackAnim();*/
    }

    /*public void RunAnim() // На случай если будешь добавлять Blend Tree, он уже есть в аниматоре. Возле выхода
    {
        if(Input.GetKey(KeyCode.Z))
        {
            animator.SetBool("Run", true);
        }
        else
        {
            animator.SetBool("Run", false);
        }
    }*/
    
    public override void Run()
         {
             curSpeed = agent.velocity.magnitude / speed;
             animator.SetFloat("Blend", curSpeed);
             //animator.SetBool("Run", true);
         }
    
    public void Idle()
    {
        animator.SetBool("Run", false);
    }
    /*public void DieAnim()
    {
        if (Input.GetKey(KeyCode.X)) //if(health <= 0) активирушь метод. Там где, объект удаляется поставить таймер, чтобы анимация успела проиграться.
        {
            animator.SetBool("Die", true);
        }
        else
        {
            animator.SetBool("Die", false);
        }
    }*/

    public override void Die()
    {
        animator.SetBool("Die", true);
        StartCoroutine(StopDie());
    }

    private IEnumerator StopDie()
    {
        yield return new WaitForSeconds(2f);
        animator.SetBool("Die", false);
    }
    public void DamageAnim() //проверка на урон
    {
        if (Input.GetKey(KeyCode.C))
        {
            animator.SetBool("Damage", true);
        }
        else
        {
            animator.SetBool("Damage", false);
        }
    }
    
    public override void TakeDamage()
    {
        animator.SetBool("Damage", true);
        StartCoroutine(StopDamage());
    }

    private IEnumerator StopDamage()
    {
        yield return new WaitForSeconds(2f);
        animator.SetBool("Damage", false);
    }
    public void AttackAnim()
    {
        if (Input.GetKey(KeyCode.V)) //при столкновении коллайдерев игрока и моба
        {
            animator.SetBool("Attack", true);
        }
        else
        {
            animator.SetBool("Attack", false);
        }
    }
    
    public override void Attack()
    {
        animator.SetBool("Attack", true);
        StartCoroutine(StopAttack());
    }

    private IEnumerator StopAttack()
    {
        yield return new WaitForSeconds(2f);
        animator.SetBool("Attack", false);
    }
}
